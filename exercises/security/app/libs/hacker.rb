# frozen_string_literal: true

require 'open-uri'
require 'nokogiri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      host = 'https://rails-l4-collective-blog.herokuapp.com'
      form_path = '/users/sign_up'
      action_path = '/users'
      page = URI.parse("#{host}#{form_path}").open
      token = Nokogiri::HTML(page).at('input[@name="authenticity_token"]')['value']
      cookie = page.meta['set-cookie']
      uri = URI.parse("#{host}#{action_path}")
      params = {
        authenticity_token: token,
        user: {
          email: email,
          password: password,
          password_confirmation: password
        } 
      }
      headers = { 'Cookie' => cookie }

      Net::HTTP.post(uri, params.to_query, headers)
      # END
    end
  end
end
