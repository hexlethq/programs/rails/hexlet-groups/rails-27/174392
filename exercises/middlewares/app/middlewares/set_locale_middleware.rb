# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    accepted_languages = env['HTTP_ACCEPT_LANGUAGE'] || I18n.default_locale
    Rails.logger.debug "* Accept-Language: #{accepted_languages}"
    locale = extract_locale_from_accept_header(env)
    Rails.logger.debug "* Locale set to '#{locale}'"
    switch_locale(locale)

    app.call(env)
  end

  private

  def extract_locale_from_accept_header(env)
    env['HTTP_ACCEPT_LANGUAGE']&.scan(/^[a-z]{2}/)&.first
  end

  def switch_locale(locale)
    I18n.locale = locale || I18n.default_locale
  end
  # END
end
