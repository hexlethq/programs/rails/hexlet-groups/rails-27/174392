# frozen_string_literal: true

require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  def setup
    @task = tasks(:one)
  end

  test 'valid task' do
    assert @task.valid?
  end

  test 'invalid without name' do
    @task.name = nil
    assert_not_nil @task.errors[:name]
  end

  test 'invalid without status' do
    @task.status = nil
    assert_not_nil @task.errors[:status]
  end

  test 'invalid without creator' do
    @task.creator = nil
    assert_not_nil @task.errors[:created]
  end

  test 'invalid without completed' do
    @task.completed = nil
    assert_not_nil @task.errors[:completed]
  end
end
