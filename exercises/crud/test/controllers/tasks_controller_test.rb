# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  def setup
    @task1 = tasks(:one)
    @task2 = tasks(:two)
    @valid_params = { name: 'Valid task', status: 'new', creator: 'Me', completed: false }
    @invalid_params = { name: 'Invalid task', completed: true }
  end

  test 'index action' do
    get tasks_path
    assert_response :success
    assert_select 'section ul li', 2
    assert_select 'section ul li:first-child a', @task1.name
  end

  test 'show action' do
    get task_path(@task1)
    assert_response :success
    assert_select 'h1', @task1.name
  end

  test 'new action' do
    get new_task_path
    assert_response :success
    assert_select 'h1', 'New task'
  end

  test 'create action success' do
    post tasks_path, params: {
      task: @valid_params
    }
    assert_response :redirect
    follow_redirect!
    assert_select 'h1', @valid_params[:name]
  end

  test 'create action failure' do
    post tasks_path, params: {
      task: @invalid_params
    }
    assert_response :success
    assert_select 'h1', 'New task'
  end

  test 'get update' do
    get edit_task_url(@task1)
    assert_response :success
    assert_select 'h1', 'Edit task'
  end

  test 'update action success' do
    patch task_path(@task1), params: {
      task: @valid_params
    }
    assert_response :redirect
    follow_redirect!
    assert_select 'h1', 'Valid task'
  end

  test 'update action failure' do
    patch task_path(@task1), params: {
      task: @invalid_params
    }
    assert_response :redirect
  end

  test 'destroy action' do
    delete task_url(@task1)
    assert_redirected_to tasks_path
    assert_equal 1, Task.count
  end
end
