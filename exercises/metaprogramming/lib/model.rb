# frozen_string_literal: true

# BEGIN
module Model
  # called when the module is included to class
  def self.included(base)
    base.extend(ClassMethods)
  end

  # delegate initialize method to extended class
  def initialize(attrs = {})
    @params = {}
    self.class.schema.each do |name, options|
      if attrs.key?(name)
        @params[name] = transform(attrs[name], options[:type])
      else
        @params[name] = nil
      end
    end
  end

  def attributes
    @params
  end

  TRANSFORMING = {
    string: ->(value) { value.to_s },
    integer: ->(value) { value.to_i },
    boolean: ->(value) { !value == false },
    datetime: ->(value) { DateTime.parse(value) }
  }.freeze

  def transform(value, type)
    return value if value.nil?

    TRANSFORMING[type].call(value)
  end

  module ClassMethods
    attr_accessor :params
    attr_reader :schema

    def attribute(name, options = {})
      @schema ||= {}
      @schema[name] = options

      # Getter
      define_method name do
        @params[name]
      end
      ########

      # Setter
      define_method "#{name}=" do |value|
        @params[name] = transform(value, options[:type])
      end
      ########
    end
  end
end

# END
