# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
    @post2 = posts(:two)
  
    visit posts_url
  end

  test 'visiting the index' do
    assert_selector 'h1', text: 'Posts'
  end

  test 'visiting the Post' do
    find('tr', text: @post.title).click_link('Show')

    assert_selector 'p', text: @post.body
  end

  test 'creating a Post' do
    click_on 'New Post'
    fill_in 'Title', with: @post.title
    fill_in 'Body', with: @post.body
    click_on 'Create'

    assert_text 'Post was successfully created'
    assert_selector 'p', text: @post.body
  end

  test 'creating a Comment' do
    find('tr', text: @post.title).click_link('Show')
    fill_in 'post_comment_body', with: 'New Comment'
    click_on 'Create Comment'

    assert_text 'Comment was successfully created'
    assert_selector 'small', text: 'New Comment'
  end

  test 'updating a Post' do
    find('tr', text: @post.title).click_link('Edit')
    fill_in 'Title', with: @post2.title
    click_on 'Update Post'

    assert_text 'Post was successfully updated'
    click_on 'Back' 
  end

  test 'updating a Post with failure' do
    find('tr', text: @post.title).click_link('Edit')
    fill_in 'Title', with: ''
    click_on 'Update Post'
    
    assert_text "Title can't be blank"
    click_on 'Back' 
  end

  test 'destroying a Post' do
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Post was successfully destroyed.'
  end
end
# END
