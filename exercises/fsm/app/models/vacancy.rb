# frozen_string_literal: true

class Vacancy < ApplicationRecord
  include AASM

  validates :title, presence: true
  validates :description, presence: true

  # BEGIN
  aasm whiny_transitions: false do
    state :on_moderate, initial: true
    state :archived, :published

    event :archive do
      transitions from: %i[on_moderate published], to: :archived
    end

    event :publish do
      transitions from: :on_moderate, to: :published
    end    
  end
  # END
end
