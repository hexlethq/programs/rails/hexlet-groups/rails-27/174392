# frozen_string_literal: true

# class AdminPolicy
class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)
    handle(req)
  end

  private

  def handle(req)
    req.path.start_with?('/admin') ? [403, {}, []] : @app.call(req.env)
  end
end
