# frozen_string_literal: true

require 'digest'

# class Siganture
class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, _, body = @app.call(env)
    encoded_body = encode(body)
    [status, {}, encoded_body]
  end

  private

  def encode(body)
    body_hash = Digest::SHA256.hexdigest(body)
    [body, body_hash].join("\n")
  end
end
