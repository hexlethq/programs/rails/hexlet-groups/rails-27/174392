# frozen_string_literal: true

# class Router
class Router
  def call(env)
    req = Rack::Request.new(env)
    handle(req)
  end

  private

  def handle(req)
    case req.path
    when '/'
      [200, {}, 'Hello, World!']
    when '/about'
      [200, {}, 'About page']
    else
      [404, {}, '404 Not Found']
    end
  end
end
