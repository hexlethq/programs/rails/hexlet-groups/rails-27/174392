# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new(%w[ruby js rust])
  end

  def test_pop!
    empty_stack = Stack.new
    assert_nil(empty_stack.pop!)
    assert_equal('rust', @stack.pop!)
  end

  def test_push!
    assert_equal(%w[ruby js rust go], @stack.push!('go'))
  end

  def test_empty?
    new_stack = Stack.new
    assert_equal(false, @stack.empty?)
    assert_equal(true, new_stack.empty?)
  end

  def test_to_a
    assert_equal(%w[ruby js rust], @stack.to_a)
  end

  def test_clear!
    assert_equal([], @stack.clear!)
  end

  def test_size
    stack = Stack.new(%w[rock scissors paper])
    assert_equal(3, stack.size)
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
