# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    before_action :set_comment, only: %i[edit update destroy]
    before_action :set_post

    def create
      @comment = @post.post_comments.build(comment_params)
      if @comment.save
        redirect_to @post
      else
        render 'posts/show'
      end
    end

    def edit; end

    def update
      if @comment.update(comment_params)
        redirect_to @comment.post
      else
        render :edit
      end
    end

    def destroy
      @comment.destroy
      redirect_to @post
    end

    private

    def set_comment 
      @comment = PostComment.find(params[:id])
    end

    def set_post
      @post = Post.find(params[:post_id])
    end

    def comment_params
      params.require(:post_comment).permit(:body)
    end
  end
end
