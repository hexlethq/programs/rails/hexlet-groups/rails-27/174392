# frozen_string_literal: true

# BEGIN
def get_same_parity(arr)
  return arr if arr.empty?

  if arr.first.even?
    arr.select(&:even?)
  else
    arr.select(&:odd?)
  end
end
# END
