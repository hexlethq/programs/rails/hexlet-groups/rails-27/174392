# frozen_string_literal: true

# BEGIN
def sort_by_chars(str)
  str.chars.sort.join
end

def anagramm_filter(word, words_list)
  word_sorted = sort_by_chars(word)
  words_list.select { |c| word_sorted == sort_by_chars(c) }
end
# END
