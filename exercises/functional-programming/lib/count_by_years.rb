# frozen_string_literal: true

# BEGIN
def filter_men(arr)
  arr.filter { |hash| hash[:gender] == 'male' }
end

def get_years(arr)
  arr.map { |hash| hash[:birthday].split('-').first }
end

def count_by_years(arr)
  filtered = filter_men(arr)
  years_arr = get_years(filtered)
  years_arr.each_with_object(Hash.new(0)) { |year, counts| counts[year] += 1 }
end
# END
