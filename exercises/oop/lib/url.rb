# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

class Url
  include Comparable
  extend Forwardable
  attr_reader :uri

  def_delegators :@uri, :scheme, :host

  def initialize uri
    @uri = URI(uri)
  end

  def query_params
    @uri.query.split('&')
      .map { |w| w.split('=') }
      .map { |a| [a[0].to_sym, a[1]] }
      .to_h
  end

  def query_param(param, default_value=nil)
    params = self.query_params
    params.include?(param) ? params[param] : default_value
  end

  def <=> other
    self.uri <=> other.uri
  end
end
# END
