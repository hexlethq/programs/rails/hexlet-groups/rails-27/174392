# frozen_string_literal: true

# BEGIN

def reverse(str)
  result = []
  splitted = str.split('')
  str.size.times { result << splitted.pop }
  result.join
end

# END
