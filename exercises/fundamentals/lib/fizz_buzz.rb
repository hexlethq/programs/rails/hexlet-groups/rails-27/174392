# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  result = []
  (start..stop).each do |num|
    if (num % 3).zero? && (num % 5).zero?
      result << 'FizzBuzz'
    elsif (num % 3).zero?
      result << 'Fizz'
    elsif (num % 5).zero?
      result << 'Buzz'
    else
      result << num
    end
  end
  result.join(' ')
end
# END
