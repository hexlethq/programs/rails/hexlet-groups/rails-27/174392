# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  if num.negative?
    nil
  elsif num <= 2
    num - 1
  else
    fibonacci(num - 1) + fibonacci(num - 2)
  end
end
# END
