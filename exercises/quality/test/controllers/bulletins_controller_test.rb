# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'index' do
    get bulletins_path
    assert_response :success
    assert_select 'li', 'Medical bulletin'
    assert_select 'li', 'Dentists bulletin'
  end

  test 'show' do
    get bulletin_path(1)
    assert_response :success
    assert_select 'p', 'Medical bulletin description'
  end
end
