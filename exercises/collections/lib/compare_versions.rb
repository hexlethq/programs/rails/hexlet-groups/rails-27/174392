# frozen_string_literal: true

# BEGIN
def str_to_arr(str)
  str.split('.').map(&:to_i)
end

def compare_versions(version1, version2)
  a1 = str_to_arr(version1)
  a2 = str_to_arr(version2)
  a1 <=> a2
end
# END
