# frozen_string_literal: true

# rubocop:disable Style/For

# BEGIN
def make_censored(text, stop_words)
  text.split(' ').map do |w|
    stop_words.include?(w) ? "$#%!" : w
  end.join(' ')
end
# END

# rubocop:enable Style/For
