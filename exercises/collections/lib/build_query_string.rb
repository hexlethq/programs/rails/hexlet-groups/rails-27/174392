# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string query_object
  sorted = query_object.sort_by { |k, v| k}
  sorted.map { |k, v| "#{k.to_s}=#{v.to_s}" }.join("&")
end
# END
# rubocop:enable Style/For
